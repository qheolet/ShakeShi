import { Injectable } from 'angular2/core';


@Injectable()
export class dataService {
    data = {
        col_one: ['Silent', 'nude', 'French', 'Cockney', 'Stalinist', 'Drunk'],
        col_two: ['Kung-fu', 'Bicycling', 'Robot', 'Zombie', 'Hip Hop', 'Cowboy'],
        col_three: ['Hamlet', 'Macbeth', 'Richard', 'Othello', 'King Lear', 'Romeo & Juliet'],
        col_four: ['With a new ending', 'On mars', 'Aboard a speedboat', 'on wall street', 'with dinosaurs', 'in a car park']
    };

    constructor() { }

    get() {
        return this.data;
    }
}