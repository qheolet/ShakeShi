import { Component, Input, Output, EventEmitter } from 'angular2/core';
import { IminiTag } from './interfaces';

@Component({
    selector: 'shake-speak',
    templateUrl: './shake-speak.html',
})

export class shakeSpeak{
    @Input() datatag;
    @Output() deleteTagNotifier = new EventEmitter<IminiTag>();

    private text: string = "CHOOSE ONE ITEM FROM EACH COLUMN TO CREATE AN INNOVATIVE PRODUCTION IDEA!";

    deleteTag(info: IminiTag): void {
        this.deleteTagNotifier.emit(info);
    }
}

