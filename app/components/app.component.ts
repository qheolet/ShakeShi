import {Component, OnInit} from 'angular2/core';
import {tagList} from './app.tagList';
import {shakeSpeak} from './app.speak';
import { minitagServices } from './services.selected';
import { IminiTag }  from './interfaces';
import { arrayObjectIndexOf } from './utilities';
import { Observable } from 'rxjs/Rx';
import { dataService } from './app.data.services';



@Component({
    selector: 'shakeshi',
    templateUrl: './shakeshi.html',
    directives: [tagList, shakeSpeak],
    providers: [minitagServices, dataService]
})

export class AppComponent implements OnInit {
    data: any;
    datatags: any;
    memory: any;

    constructor(private _minitagServices: minitagServices, private _dataService: dataService) {
        this.datatags = [];
        this.memory = [];

        this.data = _dataService.get();

    }

    ngOnInit() {
    }

    addTagToSpeak(e) {
        this.datatags = this._minitagServices.get();
        if (this.memory.indexOf(e.column) == -1) {
            this.memory.push(e);
            this._minitagServices.add(e);
        }
    }

    deleteTagParent(e: IminiTag) {
        this.memory.splice(arrayObjectIndexOf(this.memory, e.column, 'column'), 1)
        this._minitagServices.erase(e);
        const element: HTMLElement = <HTMLElement>document.getElementsByClassName(e.column)[1];
        const children: HTMLElement[] = element.children;

        element.style.opacity = '1';
        for (var index = 0; index < children.length; index++) {
            children[index].style.opacity = '1';
        }
    }
}