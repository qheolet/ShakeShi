import { Injectable } from 'angular2/core';
import { IminiTag } from './interfaces';
import { arrayObjectIndexOf } from './utilities';


@Injectable()
export class minitagServices {
    private minitag: any[];

    constructor() {
        this.minitag = [];
    }

    add(obj: IminiTag): void {
        if (this.minitag.length < 4) {
            this.minitag.push(obj);
            this.minitag.sort(function(a,b) { return a.index - b.index });
        }
    }

    erase(e: IminiTag): void{
        this.minitag.splice(arrayObjectIndexOf(this.minitag,e.tag,'tag'), 1);
    }

    get(): IminiTag[] {
        return this.minitag;
    }
}