import { Component, OnInit, Input, Output, EventEmitter } from 'angular2/core';
import { minitagServices } from './services.selected';
import { arrayObjectIndexOf } from './utilities';
import { IminiTag } from './interfaces';
import { Observable } from 'rxjs/Rx';

@Component({
    selector: 'tags-list',
    templateUrl: 'tags-list.html',
})

export class tagList implements OnInit {
    @Input() object;
    @Input() memory: IminiTag[];
    @Output() tagSelected = new EventEmitter<any>();

    private keys: string[];
    private Array: string[];
    private index: number;

    constructor(private _minitagServices:minitagServices) {
    }


    ngOnInit() {
        this.keys = Object.keys(this.object);
        this.Array = this.keys.map((val) => {
            return this.object[val];
        })
    }

    clickTag(column, tag, index, MouseEvent: MouseEvent) {
        const element: HTMLElement = <HTMLElement>MouseEvent.target;
        const parentElement: HTMLElement = element.parentElement;
        const opacity = window.getComputedStyle(element).opacity;

        if (arrayObjectIndexOf(this.memory,column,'column')  == -1) {
            element.style.opacity = '0.33';
            parentElement.style.opacity = '0.50';
            this.tagSelected.emit({ column, tag, index });
        }

    }
}