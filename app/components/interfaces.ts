export interface IminiTag {
    column: string;
    tag: string;
    index: number;
}