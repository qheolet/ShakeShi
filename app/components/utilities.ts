export function arrayObjectIndexOf(myArray: {}[], searchTerm:any, property: string) {
    for(var i = 0, len = myArray.length; i < len; i++) {
        if (myArray[i][property] === searchTerm) return i;
    }
    return -1;
}
